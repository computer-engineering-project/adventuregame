# AdventureGame

## Project: Simple Virtual Tabletop Environment

089020 - PROGETTO DI INGENERIA INFORMATICA

Group members:
Hovland_Hedda_Lockert_10991899 ; Tysland_Helena_Steine_10991173 ; Haugland_Mari_Skjærpe_10990873

Supervisor:
Agosta, Giovanni 

### Documentations:

Included in the ZIP-file is
- README.txt, this file.
- a PDF, this is the Report with a detailed documentation of the project, and link to the tutorial and gitlab.
- Tutorial.mp4, this is a video tutorial of how to use the webapplication and how to play the game. 

## Running the program: 

After downloading the repository it is important to run an innstallment to ensure all packages and libraries are installed. 

In the project directory;
Start server:

```
cd Server
npm i 
npm start
```

Start the app:
```
cd adventure-project
npm i 
npm start
```
## Running the tests: 

There are Unit-tests for the different pages. 
To run the tests:

````
cd adventure-project
npm i
npm test
````





