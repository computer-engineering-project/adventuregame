import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import SecondWait from '../Pages/SecondWait';
import { LevelContext } from '../Components/LevelContext';

test('renders SecondWait component', () => {

  const mockSetChosenLevel = jest.fn();

  // Provide the default context value for testing
  const defaultContextValue = {
    chosenLevel: 3, // or any default value you want
    setChosenLevel: mockSetChosenLevel,
  };

  const { getByText } = render(
    <Router>
     <LevelContext.Provider value={defaultContextValue}>
        <SecondWait />
      </LevelContext.Provider>
    </Router>
  );

  // Check if specific text is present
  const adminText = screen.getByText(/Players/i);
  expect(adminText).toBeInTheDocument();
});

