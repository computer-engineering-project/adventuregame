import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import Home from '../Pages/Home';
import { LevelContext } from '../Components/LevelContext';

test('renders Home component', () => {

  const mockSetChosenLevel = jest.fn();

  // Provide the default context value for testing
  const defaultContextValue = {
    chosenLevel: 3, // or any default value you want
    setChosenLevel: mockSetChosenLevel,
  };

  const { getByText } = render(
    
      <Router>
        <LevelContext.Provider value={defaultContextValue}>
          <Home />
        </LevelContext.Provider>

      </Router>
  );


    // Check if specific text are present
    const editText = screen.getByText(/Welcome to Adventure Odyssey!/i);
    expect(editText).toBeInTheDocument();
    

  // Simulate clicking the button for Level 1
  fireEvent.click(getByText('Water level'));

  // Verify setChosenLevel was called with 'Level 1'
  expect(mockSetChosenLevel).toHaveBeenCalledWith(1);
});



