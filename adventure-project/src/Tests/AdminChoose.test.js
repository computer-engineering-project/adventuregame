import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import AdminChoose from '../Pages/AdminChoose';

test('renders AdminChoose component', () => {
  render(
    <Router>
      <AdminChoose />
    </Router>
  );

  // Check if specific text are present
  const chooseText = screen.getByText(/Choose/i);
  expect(chooseText).toBeInTheDocument();
});
