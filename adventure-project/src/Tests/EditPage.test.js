import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import EditPage from '../Pages/EditPage';

test('renders EditPage component', () => {
  render(
    <Router>
      <EditPage />
    </Router>
  );

  // Check if specific text are present
  const editText = screen.getByText(/Click on the grid to paint/i);
  expect(editText).toBeInTheDocument();
});
