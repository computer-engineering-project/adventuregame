import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter as Router } from 'react-router-dom';
import Admin from '../Pages/Admin';

test('renders Admin component', () => {
  render(
    <Router>
      <Admin />
    </Router>
  );

  // Check if specific text is present
  const adminText = screen.getByText(/Admin Log In/i);
  expect(adminText).toBeInTheDocument();
});
