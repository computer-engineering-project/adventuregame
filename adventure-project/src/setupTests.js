import '@testing-library/jest-dom/extend-expect';

// Create a mock for the DOM element
beforeAll(() => {
  const root = document.createElement('div');
  root.setAttribute('id', 'root');
  document.body.appendChild(root);
});

// Mock WebSocket
global.WebSocket = require('./__mocks__/websocket');

// Mock the `index.js` module to avoid running its actual code during tests
jest.mock('./index', () => {
  const mockWebSocket = jest.fn(() => ({
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    send: jest.fn(),
    close: jest.fn(),
    readyState: 1,
  }));

  return {
    getWebSocketInstance: jest.fn(() => mockWebSocket),
  };
});
