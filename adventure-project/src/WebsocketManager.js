// WebSocketManager.js
import React, { useEffect, useState } from "react";
import { getWebSocketInstance } from "./index.js";

const WebSocketManager = ({ children }) => {
  const [userList, setUserList] = useState([]);

  useEffect(() => {
    const ws = getWebSocketInstance(); // Get WebSocket instance

    ws.onmessage = function (event) {
      const data = JSON.parse(event.data);
      if (data.type === "userList") {
        setUserList(data.users);
      }
    };

    return () => {
      // Cleanup function to unsubscribe from WebSocket events
      ws.onmessage = null;
    };
  }, []);

  return React.Children.map(children, (child) =>
    React.cloneElement(child, {
      userList: userList,
    })
  );
};

export default WebSocketManager;
