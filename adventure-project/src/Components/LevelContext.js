// LevelContext.js
import React, { createContext, useState } from "react";

const LevelContext = createContext();

const LevelProvider = ({ children }) => {
  const [chosenLevel, setChosenLevel] = useState(null);

  return (
    <LevelContext.Provider value={{ chosenLevel, setChosenLevel }}>
      {children}
    </LevelContext.Provider>
  );
};

export { LevelProvider, LevelContext };
