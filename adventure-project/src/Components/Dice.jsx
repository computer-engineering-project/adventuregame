import React, { useState } from "react";
import { getWebSocketInstance } from "../index.js";

const Dice = ({ ws }) => {
  //const ws = getWebSocketInstance();

  const [number, setNumber] = useState("1");
  const [feedback, setFeedback] = useState("");
  const [hasRolled, setHasRolled] = useState(false);

  const moveToken = () => {
    if (hasRolled) {
      const serverMessage = JSON.stringify({
        type: "playerMoved",
        result: number,
      });
      ws.send(serverMessage);
      setHasRolled(false);
    }
  };

  const sendRollToServer = (value) => {
    // Send message to server with userId and username¨
    const serverMessage = JSON.stringify({
      type: "diceResult",
      diceResult: value,
    });
    ws.send(serverMessage);
  };

  const rollDice = () => {
    if (!hasRolled) {
      setFeedback("");
      const randomNumber = Math.floor(Math.random() * 6) + 1;
      setNumber(randomNumber);
      setFeedback(`You rolled ${randomNumber}!`);
      sendRollToServer(randomNumber);
      setHasRolled(true);
    }
  };

  return (
    <div>
      <h2>Roll the dice</h2>
      <h3>{feedback}</h3>
      <div className="dice" onClick={rollDice}>
        {number}
      </div>
      <button className="moveTokenButton" onClick={moveToken}>
        Move
      </button>
    </div>
  );
};

export default Dice;
