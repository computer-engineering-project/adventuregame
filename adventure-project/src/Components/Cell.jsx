import React, { Component, useState } from "react";
import Avatar from "./Avatar";

const Cell = ({ id, onClick, players, cellColor, touchable, isEmpty }) => {
  // const [color, setCellColor] = useState(cellColor);
  const handleClick = () => {
    if (touchable) {
      onClick(id);
    }
  };

  const generateColorFromId = (id) => {
    // Convert the player ID to a numeric value
    const numericId = parseInt(id.replace(/\D/g, ""), 10);
    const hue = (numericId * 30) % 360; // Adjust 30 as needed to change color variation
    return `hsl(${hue}, 70%, 50%)`; // Use HSL color format with a fixed saturation and lightness
  };
  // Inside the playerCircles mapping
  const playerCircles = players
    ? players.map((player) => {
        if (player && player.id && player.username) {
          const playerColor = generateColorFromId(player.id);
          return (
            <Avatar
              key={player.id}
              player={player.username}
              color={playerColor}
            ></Avatar>
          );
        } else {
          return null;
        }
      })
    : null;

  const content = isEmpty ? null : playerCircles;

  const cellStyle = {
    width: "60px", // Adjust cell width as needed
    height: "60px", // Adjust cell height as needed
    backgroundColor: cellColor, // Default background color
    border: "1px solid #000", // Border around each cell
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: touchable ? "pointer" : "default",
  };

  return (
    <div className="cell" style={cellStyle} onClick={handleClick}>
      {content}
    </div>
  );
};

export default Cell;
