import React, { useState } from "react";
import "../App.css";
import Confetti from "react-confetti";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";

const Goal = ({ pw }) => {
  // pop-up window visibility
  //const [showPopup, setShowPopup] = useState(true);

  /*
  const togglePopup = () => {
    setShowPopup(!showPopup);
  };*/

  return (
    <div>
      <Confetti width={window.innerWidth} height={window.innerHeight} />
      <div className="popup_w">
        <div className="popup-win">
          <h1>{pw} won! </h1>
          <div>
            <Link className="back_w" to="/home">
              <p> Back to homescreen</p>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Goal;
