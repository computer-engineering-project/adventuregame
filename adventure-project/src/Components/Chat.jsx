import React, { useState, useEffect } from "react";
import Dice from "./Dice";
import "../GameRoom.css";
import { getWebSocketInstance } from "../index.js";

// Chat.js
const Chat = ({ ws }) => {
  const [currentPlayer, setCurrentPlayer] = useState();
  const [currentResult, setCurrentResult] = useState();
  const [yourTurn, setYourTurn] = useState();
  //const ws = getWebSocketInstance();

  useEffect(() => {
    const ws = getWebSocketInstance();
    ws.addEventListener("message", handleChatMessage);
    return () => {
      ws.removeEventListener("message", handleChatMessage);
    };
  }, []);

  const handleChatMessage = (event) => {
    const data = JSON.parse(event.data);
    if (data.type === "yourTurn") {
      console.log("Your turn");
      setYourTurn(true);
    } else if (data.type === "anotherPlayer") {
      console.log(data.currentPlayer);
      setYourTurn(false);
      setCurrentResult("");
      setCurrentPlayer(data.currentPlayer);
    } else if (data.type === "diceResults") {
      setCurrentResult(data.diceResult);
      console.log(data.diceResult);
      if (yourTurn) {
        console.log("You are moving on the board");
        //move(data.diceResult);
      }
    }
    // Add your message handling logic here
  };

  useEffect(() => {
    const serverMessage = JSON.stringify({
      type: "allInRoom",
    });
    ws.send(serverMessage);
  }, []);

  return (
    <div className="chatField">
      <h3>Chat</h3>
      <h3>{yourTurn ? "Your turn" : `${currentPlayer} 's turn`}</h3>
      {yourTurn ? (
        <Dice ws={ws} />
      ) : currentResult === "" ? (
        `Waiting for ${currentPlayer} to roll ...`
      ) : (
        `${currentPlayer} rolled ${currentResult}`
      )}
    </div>
  );
};

export default Chat;
