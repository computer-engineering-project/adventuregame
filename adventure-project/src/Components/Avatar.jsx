import React, { Component, useState } from "react";

const Avatar = ({ position, player, color }) => {
  return (
    <div
      style={{
        position: "absolute",
        width: "40px",
        height: "40px",
        borderRadius: "50%",
        backgroundColor: color,
        zIndex: 1, // Set a zIndex to ensure avatars stack on top of each other
      }}
    >
      {player[0]}
    </div>
  );
};
export default Avatar;
