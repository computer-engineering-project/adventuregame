import React, { Component, useState, useEffect } from "react";
import Cell from "./Cell.jsx";
import "../GameRoom.css";
import { getWebSocketInstance } from "../index.js";
import terrainCoordinates from "./Worlds.js";
import Goal from "./Goal.jsx";

const Grid1 = ({ selectedColor, updateGridState, admin }) => {
  const rows = 10;
  const cols = 20;

  const defaultPath = terrainCoordinates.Lava;
  // Create the initial grid state
  const [players, setPlayers] = useState([]);
  const [inGoal, setInGoal] = useState(false);
  const [winner, setWinner] = useState();

  const [grid, setGrid] = useState(() => {
    const storedGridState = localStorage.getItem("gridState4");
    if (storedGridState && storedGridState.length > 2) {
      // If gridState exists in localStorage, parse and return it
      return JSON.parse(storedGridState);
    } else {
      const initialGrid = [];
      for (let row = 0; row < rows; row++) {
        const rowCells = [];
        for (let col = 0; col < cols; col++) {
          rowCells.push({
            id: `${row}-${col}`,
            color: "transparent",
          });
        }
        initialGrid.push(rowCells);
      }
      return initialGrid;
    }
  });

  const [userPaths, setUserpaths] = useState({});

  useEffect(() => {
    const ws = getWebSocketInstance();
    if (inGoal) {
      ws.send(
        JSON.stringify({
          type: "gameFinished",
          level: 4,
        })
      );
    }
    ws.addEventListener("message", handleGridMessage);
    return () => {
      ws.removeEventListener("message", handleGridMessage);
    };
  }, [players, userPaths]);

  const isCoordinateInPath = (row, col) => {
    return defaultPath.some(([r, c]) => r === row && c === col);
  };

  const handleGridMessage = (event) => {
    const data = JSON.parse(event.data);
    if (data.type === "moveToken") {
      console.log(userPaths);
      const player = data.player;
      const result = data.result;
      setUserpaths((prevUserPaths) => {
        const oldPath = prevUserPaths[player];
        //if (defaultPath.length)

        const startIndex = defaultPath.indexOf(oldPath);

        if (startIndex + result >= defaultPath.length - 1) {
          setInGoal(true);
          setWinner(player);
          console.log(inGoal);
          return {};
        } else {
          const newPathIndex = startIndex + result;
          const newPath = defaultPath.at(newPathIndex);

          // Move to the event space first
          const newUserPaths = {
            ...prevUserPaths,
            [player]: newPath,
          };

          // After a short delay, check the cell color and move back if needed
          setTimeout(() => {
            const [row, col] = newPath;
            const cellColor = grid[row][col].color;
            if (cellColor !== "transparent") {
              const stepsBack = getStepsBack(cellColor); //Change later;
              const finalPathIndex = Math.max(0, newPathIndex - stepsBack);
              const finalPath = defaultPath.at(finalPathIndex);

              setUserpaths((prevPaths) => ({
                ...prevPaths,
                [player]: finalPath,
              }));
            }
          }, 1000); // Adjust the delay as needed

          return newUserPaths;
        }
      });
    } else if (data.type === "userList") {
      setPlayers(data.users);
      if (players.length === 0) {
        console.log(players);
        const newUserPaths = {};
        Object.entries(data.users).forEach(([id, username]) => {
          newUserPaths[id] = defaultPath.at(0);
        });
        setUserpaths(newUserPaths);
      } else {
        const newUserPaths = { ...userPaths }; // Copying the existing user paths

        // Finding the element of players that is not in the new user list
        Object.keys(newUserPaths).forEach((id) => {
          if (!(id in data.users)) {
            delete newUserPaths[id]; // Deleting the user path
          }
        });

        setUserpaths(newUserPaths);
      }
    }
  };

  const getStepsBack = (color) => {
    switch (color) {
      case "#db1f2b":
        return 5;
      case "#b1ed8a":
        return -2;
      case "#62b82a":
        return -4;
      case "#f08d30":
        return 3;
      default:
        return 0;
    }
  };

  // Function to handle cell click
  const handleCellClick = (row, col) => {
    // Update the color of the clicked cell
    if (admin) {
      setGrid((prevGrid) => {
        const newGrid = [...prevGrid];
        newGrid[row][col].color = selectedColor;
        updateGridState(newGrid);
        return newGrid;
      });
    }
  };

  const playersInCell = (row, col) => {
    let playersCell = [];

    Object.entries(userPaths).forEach(([id, path]) => {
      if (path[0] === row && path[1] === col) {
        playersCell.push({ id, username: players[id] });
      }
    });

    return playersCell;
  };
  return (
    console.log("Rendering Grid1"),
    inGoal ? (
      <Goal pw={players[winner]} />
    ) : (
      <div className="grid4">
        {grid.map((row, rowIndex) => (
          <div key={rowIndex} style={{ display: "flex" }}>
            {row.map((cell, colIndex) => (
              <Cell
                key={cell.id}
                id={cell.id}
                cellColor={cell.color}
                onClick={() => handleCellClick(rowIndex, colIndex)}
                players={playersInCell(rowIndex, colIndex)}
                touchable={isCoordinateInPath(rowIndex, colIndex)}
                isEmpty={playersInCell(rowIndex, colIndex).length === 0}
              />
            ))}
          </div>
        ))}
      </div>
    )
  );
};

export default Grid1;
