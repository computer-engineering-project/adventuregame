import React from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import "../App.css";
import "../Home.css";
import level1 from "../Icons/water.png";
import level2 from "../Icons/space.png";
import level3 from "../Icons/forest.png";
import level4 from "../Icons/Lava.png";

const Home = () => {
  return (
    <div className="main-content">
      <div className="adminheader">
        <h1>Admin-page</h1>
        <p>Here you can choose which level you would like to edit:</p>
      </div>
      <div className="levels">
        <div>
          <Link to="/editpage?level=1">
            <h2>Water level</h2>
            <img src={level1} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/editpage?level=2">
            <h2>Space level</h2>
            <img src={level2} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/editpage?level=3">
            <h2>Forest level</h2>
            <img src={level3} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/editpage?level=4">
            <h2>Lava level</h2>
            <img src={level4} alt="" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
