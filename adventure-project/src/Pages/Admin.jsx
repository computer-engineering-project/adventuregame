import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes, Link, Navigate} from "react-router-dom";
import "../Admin.css";
import "../App.css";

const Admin = () => {
  const [inputWord, setInputWord] = useState("");
  const [isMatch, setIsMatch] = useState(null);
  const correctWord = "h"; // The Password

  const handleInputChange = (event) => {
    setInputWord(event.target.value);
    setIsMatch(null);
  };

  const handleButtonClick = () => {
    if (inputWord.toLowerCase() === correctWord.toLowerCase()) {
      setIsMatch(true);
    } else {
      setIsMatch(false);
    }
  };
  return (
    <div className="admin-content">
      <h1>Admin Log In</h1>
      <div className="description">
        <p>As an Admin you can log in and edit the event spaces for each world.</p>
        <div className="enter-p">
          <input
            type="text"
            value={inputWord}
            onChange={handleInputChange}
            placeholder="Enter password"
          />
          <button onClick={handleButtonClick}>Enter</button>
          {isMatch === false && inputWord !== "" ? <p>Wrong password</p> : null}
          {isMatch === true && <Navigate to="/choose" />}
        </div>
      </div>
    </div>
  );
};

export default Admin;
