import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import Grid1 from "../Components/Grid1";
import Grid2 from "../Components/Grid2";
import Grid3 from "../Components/Grid3";
import Grid4 from "../Components/Grid4";
import "../GameRoom.css";

const EditPage = () => {
  const [selectedColor, setSelectedColor] = useState("#000000");
  const [gridState, setGridState] = useState([]);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const level = searchParams.get("level");

  // pop-up window visibility
  const [showPopup, setShowPopup] = useState(false);

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  // Handler for selecting a color
  const handleColorSelect = (color) => {
    setSelectedColor(color);
  };

  const handleSave = () => {
    if (level == "1") {
      localStorage.setItem("gridState1", JSON.stringify(gridState));
      console.log("Grid state saved:", gridState);
    }
    if (level == "2") {
      localStorage.setItem("gridState2", JSON.stringify(gridState));
      console.log("Grid state saved:", gridState);
    }
    if (level == "3") {
      localStorage.setItem("gridState3", JSON.stringify(gridState));
      console.log("Grid state saved:", gridState);
    }
    if (level == "4") {
      localStorage.setItem("gridState4", JSON.stringify(gridState));
      console.log("Grid state saved:", gridState);
    }
  };

  // Function to update grid state in EditPage
  const updateGridState = (newGridState) => {
    setGridState(newGridState);
  };

  useEffect(() => {
    /*
    // Establish WebSocket connection when component mounts
    const ws = new WebSocket("ws://localhost:4000"); // WebSocket server URL
    setWs(ws);
*/
    const savedGridState = localStorage.getItem("gridState");
    if (savedGridState) {
      setGridState(JSON.parse(savedGridState));
    }
  }, []);

  return (
    <div>
      <div className="bb">
        <Link to="/choose" className="backbutt">
          Go back to the levels
        </Link>
      </div>
      <div className="pallette">
        <div className="mainContent">
          <h2>Click on the grid to paint:</h2>

          {level === "1" && (
            <Grid1
              selectedColor={selectedColor}
              updateGridState={updateGridState}
              players={{}}
              admin={true}
            />
          )}
          {level === "2" && (
            <Grid2
              selectedColor={selectedColor}
              updateGridState={updateGridState}
              players={{}}
              admin={true}
            />
          )}
          {level === "3" && (
            <Grid3
              selectedColor={selectedColor}
              updateGridState={updateGridState}
              players={{}}
              admin={true}
            />
          )}
          {level === "4" && (
            <Grid4
              selectedColor={selectedColor}
              updateGridState={updateGridState}
              players={{}}
              admin={true}
            />
          )}
        </div>
        <div className="color-picker">
          <h2>Select a color:</h2>
          <div className="color-choose">
            <div>
              <p>2 forward:</p>
              <button
                className="color-option"
                style={{ backgroundColor: "#b1ed8a" }}
                onClick={() => handleColorSelect("#b1ed8a")}
              ></button>
            </div>
            <div>
              <p>4 forward:</p>
              <button
                className="color-option"
                style={{ backgroundColor: "#62b82a" }}
                onClick={() => handleColorSelect("#62b82a")}
              ></button>
            </div>
            <div>
              <p>3 backward:</p>
              <button
                className="color-option"
                style={{ backgroundColor: "#f08d30" }}
                onClick={() => handleColorSelect("#f08d30")}
              ></button>
            </div>
            <div>
              <p>5 backward:</p>
              <button
                className="color-option"
                style={{ backgroundColor: "#db1f2b" }}
                onClick={() => handleColorSelect("#db1f2b")}
              ></button>
            </div>
            <div className="removecolor">
              <p>Remove placed event spaces:</p>
              <button
                className="color-option"
                style={{ backgroundColor: "white" }}
                onClick={() => handleColorSelect("transparent")}
              ></button>
            </div>
          </div>
          <div className="buttons">
            <button className="save-button" onClick={togglePopup}>
              Event rules
            </button>
            <button className="save-button" onClick={handleSave}>
              {" "}
              Save{" "}
            </button>
          </div>
        </div>
        {showPopup && (
          <div className="popup">
            <div className="popup-content">
              <span className="close" onClick={togglePopup}>
                &times;
              </span>
              <h2>Admin Rules</h2>
              <p>
                As an admin you can change and place event spaces on the maps to
                switch the games up!{" "}
              </p>
              <p>
                To place an event space, simply press the color of the
                event space you want to place, and then click on the square you
                want. If you want to remove a placed space, simply press on the
                white and then click the square you want to remove.{" "}
              </p>
              <p>
                Here are a few guidelines on where to put the different spaces:
              </p>
              <ul>
                <li>
                  Three steps back: avoid placing a new event space 3 spaces
                  behind this one.
                </li>
                <li>
                  Five steps back: avoid placing a new event space 5 spaces
                  behind this one.
                </li>
                <li>
                  Two steps forward: avoid placing a new event space 2 spaces in
                  front of this one.
                </li>
                <li>
                  Four steps forward: avoid placing a new event space 4 spaces
                  in front of this one.
                </li>
                <li>Do not place any event spaces on start or goal</li>
              </ul>
              <p>
                Remember to click "Save" when you are happy with the new board!
              </p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default EditPage;
