import React, { Component, useContext } from "react";
import { useState, useEffect } from "react";
import Grid from "../Components/Grid1.jsx";
import Dice from "../Components/Dice";
import Chat from "../Components/Chat";
import Avatar from "../Components/Avatar";
import "../GameRoom.css";
import { getWebSocketInstance } from "../index.js";
import { LevelContext } from "../Components/LevelContext.js";
import Grid1 from "../Components/Grid1";
import Grid2 from "../Components/Grid2";
import Grid3 from "../Components/Grid3";
import Grid4 from "../Components/Grid4";

const GameRoom = () => {
  const [userList, setUserList] = useState([]);

  const { chosenLevel } = useContext(LevelContext);
  //const wsChat = new WebSocket("ws://localhost:4000");
  const ws = getWebSocketInstance();

  useEffect(() => {
    ws.addEventListener("message", handleMessageFromServer);
    return () => {
      ws.removeEventListener("message", handleMessageFromServer);
    };
  }, []);

  const handleMessageFromServer = (event) => {
    const data = JSON.parse(event.data);
    if (data.type === "userList") {
      setUserList(data.users);
    }
  };

  const disPlayUsers = () => {
    return (
      <div>
        <ul>
          {Object.entries(userList).map(([id, username]) => (
            <li key={id}>{username}</li>
          ))}
        </ul>
      </div>
    );
  };

  return (
    <div className="gameRoom">
      <div className="grids">
        <div className="grid1">
          {chosenLevel === 1 && (
            <Grid1 players={userList} selectedColor={"transparent"} ws={ws} />
          )}
        </div>

        <div className="grid2">
          {chosenLevel === 2 && (
            <Grid2 players={userList} selectedColor={"transparent"} ws={ws} />
          )}
        </div>

        <div className="grid3">
          {chosenLevel === 3 && (
            <Grid3 players={userList} selectedColor={"transparent"} ws={ws} />
          )}
        </div>
        <div className="grid4">
          {chosenLevel === 4 && (
            <Grid4 players={userList} selectedColor={"transparent"} ws={ws} />
          )}
        </div>
      </div>

      <div className="chat">
        <div>
          <h3>Players</h3>
          {disPlayUsers()}
        </div>

        <Chat ws={ws} />
      </div>
    </div>
  );
};

export default GameRoom;
