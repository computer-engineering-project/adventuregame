import React, { useContext } from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import FirstWait from "./FirstWait";
import "../App.css";
import "../Home.css";
import level1 from "../Icons/water.png";
import level2 from "../Icons/space.png";
import level3 from "../Icons/forest.png";
import level4 from "../Icons/Lava.png";
import { LevelContext } from "../Components/LevelContext";

const Home = () => {
  const { setChosenLevel } = useContext(LevelContext);

  const handleLevelSelect = (level) => {
    setChosenLevel(level);
  };
  return (
    <div className="main-content">
      <div className="welcome-text">
        <h1>Welcome to Adventure Odyssey!</h1>
        <p>
          Choose your favorite world and join other adventurers on a journey
        </p>
      </div>
      <div className="levels">
        <div>
          <Link to="/firstWait" onClick={() => handleLevelSelect(1)}>
          <h2>Water level</h2>
            <img src={level1} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/firstWait" onClick={() => handleLevelSelect(2)}>
          <h2>Space level</h2>
            <img src={level2} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/firstWait" onClick={() => handleLevelSelect(3)}>
          <h2>Forest level</h2>
            <img src={level3} alt="" />
          </Link>
        </div>
        <div>
          <Link to="/firstWait" onClick={() => handleLevelSelect(4)}>
          <h2>Lava level</h2>
            <img src={level4} alt="" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
