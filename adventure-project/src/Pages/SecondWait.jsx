import React, { useState, useEffect, useContext } from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import "../App.css";
import GameRoom from "./GameRoom";
import { useParams } from "react-router-dom";
import { getWebSocketInstance } from "../index.js";
import FirstWait from "./FirstWait.jsx";
import { useNavigate } from "react-router-dom";
import { LevelContext } from "../Components/LevelContext.js";

const SecondWait = () => {
  const [users, setUsers] = useState([]);
  const ws = getWebSocketInstance();
  const [ready, setReady] = useState(false);
  const navigate = useNavigate();
  const [inGameroom, setInGameroom] = useState(false);
  const [roomBusy, setRoomBusy] = useState(false);

  const { chosenLevel } = useContext(LevelContext);

  useEffect(() => {

    if (ws){

      ws.addEventListener("message", handleWebSocketMessage);

    // Unregister event listener when component unmounts
    return () => {
      ws.removeEventListener("message", handleWebSocketMessage);
    };

    }
    // Register event listener
    
  }, [ws]); // Ensure to include ws in the dependency array to handle changes in WebSocket instance

  useEffect(() => {
    if (Object.keys(users).length > 1) {
      setReady(true);
    } else {
      setReady(false);
    }
  }, [users]);

  const handleWebSocketMessage = (event) => {
    const data = JSON.parse(event.data);
    if (data.type === "userList") {
      setUsers(data.users);
    } else if (data.type === "enterGameRoom" && !inGameroom) {
      console.log("Message from server to be moved to gameroom");
      navigate("/gameroom");
      setInGameroom(true);
      notifyServerGameRoom();
    } else if (data.type === "roomBusy") {
      setRoomBusy(true);
      console.log("Room busy");
    } else if (data.type === "roomAvailable") {
      setRoomBusy(false);
      console.log("Room available");
    }
  };

  const notifyServerGameRoom = () => {
    const serverMessage = JSON.stringify({
      type: "gameRoomEntry",
      users: users,
      level: chosenLevel,
    });
    ws.send(serverMessage);
    console.log("Game room message sent to server");
  };

  const disPlayUsers = () => {
    return (
      <div>
        <ul>
          {Object.entries(users).map(([id, username]) => (
            <li key={id}>{username}</li>
          ))}
        </ul>
      </div>
    );
  };

  const handleEscape = () => {
    ws.send(JSON.stringify({ type: "disconnect" }));
  };

  return (
    <div className="main-content">
      {roomBusy ? (
        <div>
          Some other players are playing in this world now. Come back later or try another level!
        </div>
      ) : (
        <div>
          <h3>Players:</h3>
          {disPlayUsers()}
        </div>
      )}
      {ready && roomBusy == false ? (
        <Link to="/gameroom" onClick={notifyServerGameRoom}>
          Start the game!
        </Link>
      ) : (
        <p>Waiting for other players to start...</p>
      )}

      <Routes>
        <Route path="/gameroom" element={<GameRoom />} />
      </Routes>
    </div>
  );
};

export default SecondWait;
