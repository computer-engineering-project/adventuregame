import React, { useState, Button, useContext, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import "../App.css";
import SecondWait from "./SecondWait";
import { LevelContext } from "../Components/LevelContext.js";

import { getWebSocketInstance } from "../index.js";

const FirstWait = () => {
  const [name, setName] = useState("");

  const [avatar, setAvatar] = useState("");
  const [isValid, setIsValid] = useState(false);
  const { chosenLevel } = useContext(LevelContext);

  const ws = getWebSocketInstance();

  const validateForm = (name) => {
    setIsValid(name !== "");
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
    validateForm(e.target.value, avatar);
  };

  const sendMessageToServer = () => {
    // Send message to server with userId and username¨
    const serverMessage = JSON.stringify({
      type: "connection",
      username: name,
      level: chosenLevel,
    });
    ws.send(serverMessage);
  };

  return (
    <div className="main-content">
      <div className="menu-item login">
        <h3>Enter your name to join a game:</h3>
        <label>
          Name:
          <input type="text" value={name} onChange={handleNameChange} />
        </label>
        {isValid ? (
          <Link to="/secondWait" onClick={sendMessageToServer}>
            Go
          </Link>
        ):<p></p>}
      </div>
      <Routes>
        <Route path="/secondWait/*" element={<SecondWait />} />
      </Routes>
    </div>
  );
};

export default FirstWait;
