import "./App.css";
import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import Home from "./Pages/Home";
import Grid from "./Components/Grid1";
import Admin from "./Pages/Admin";
import FirstWait from "./Pages/FirstWait";
import SecondWait from "./Pages/SecondWait";
import EditPage from "./Pages/EditPage";
import GameRoom from "./Pages/GameRoom";
import AdminChoose from "./Pages/AdminChoose";
import Goal from "./Components/Goal";
import { LevelProvider } from "./Components/LevelContext";
import event from "./Icons/event.png";

function App() {
  // pop-up window visibility
  const [showPopup, setShowPopup] = useState(false);

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  return (
    <Router>
      <LevelProvider>
        <div className="app">
          <div className="navbar">
            <div className="menu-item">
              <button className="rules" onClick={togglePopup}>
                Rules
              </button>
            </div>
            <div className="menu-item gameName">
              <Link to="/home">Adventure Odyssey</Link>
            </div>
            <div className="menu-item login">
              <Link to="/admin">Admin</Link>
            </div>
          </div>

          {showPopup && (
            <div className="popup">
              <div className="popup-content">
                <span className="close" onClick={togglePopup}>
                  &times;
                </span>
                <h2>Goal of the game:</h2>
                <p className="midT">
                  Race against your friends, family or enemies to be the first to cross the finish
                  line in this classic board game!
                </p>
                <h3>How a round plays out</h3>
                <p>
                  The players roll the dice, in turn, and move accordingly. 
                  Watch out for the event spaces, they can give you a little boost but also send you back!
                </p>
                <h3>The end of a game</h3>
                <p>
                  When one player crosses the finish line they win and the game
                  ends. In other words second place is just the first loser!
                </p>
                <h3>Event spaces:</h3>
                <div className="ei">
                  <img src={event} alt="" className="eventimg"/>
                </div>
              </div>
            </div>
          )}

          <Routes>
            <Route index element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/secondWait" element={<SecondWait />} />
            <Route path="/firstWait" element={<FirstWait />} />
            <Route path="/editpage" element={<EditPage />} />
            <Route path="/gameroom" element={<GameRoom />} />
            <Route path="/choose" element={<AdminChoose />} />
            <Route path="/goal" element={<Goal />} />
          </Routes>
        </div>
      </LevelProvider>
    </Router>
  );
}

export default App;
