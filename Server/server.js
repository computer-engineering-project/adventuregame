const WebSocket = require("ws");
const { v4: uuidv4 } = require("uuid");

const clients = {}; // {world: {userId: connection}}
const usernames = {}; // {world: {userId: username}}
const worlds = {}; // {world: [userId1, userId2, ...]}
const worldStatus = {}; //true for available, false for not available

let currentPlayerId = {}; // {world: currentPlayerId}

const wss = new WebSocket.Server({ port: 4000 });

wss.on("connection", function (connection) {
  const userId = uuidv4();
  let userWorld = null;

  connection.on("message", function incoming(message) {
    const data = JSON.parse(message);

    if (data.type === "connection") {
      const username = data.username;
      const world = data.level;
      userWorld = world;

      console.log(worldStatus[world]);

      if (worldStatus[world] != false) {
        if (!clients[world]) {
          clients[world] = {};
          usernames[world] = {};
          worlds[world] = [];
        }

        clients[world][userId] = connection;
        usernames[world][userId] = username;
        worlds[world].push(userId);

        console.log(`${username} (${userId}) joined world ${world}.`);
        broadcastMessage(world);
      } else {
        const message = {
          type: "roomBusy",
        };
        connection.send(JSON.stringify(message));
      }
    } else if (data.type === "gameRoomEntry") {
      broadCastGameroomEntry(userWorld);
      broadcastMessage(userWorld);
      worldStatus[userWorld] = false;
    } else if (data.type === "allInRoom") {
      if (!currentPlayerId[userWorld]) {
        currentPlayerId[userWorld] = worlds[userWorld][0];
      }
      broadCastPlayersTurn(userWorld);
    } else if (data.type === "diceResult") {
      broadcastDiceResult(userWorld, data.diceResult);
    } else if (data.type === "playerMoved") {
      broadcastMoving(userWorld, data.result);
      incrCurrentPlayer(userWorld);
      broadCastPlayersTurn(userWorld);
    } else if (data.type === "gameFinished") {
      worldStatus[data.level] = true;
      const message = {
        type: "roomAvailable",
      };
      connection.send(JSON.stringify(message));
      if (clients[data.level]) {
        Object.keys(clients[data.level]).forEach((userId) => {
          const disconnectedUsername = usernames[data.level][userId];
          delete usernames[data.level][userId];
          delete clients[data.level][userId];
          worlds[data.level] = worlds[data.level].filter((id) => id !== userId);

          console.log(
            `${disconnectedUsername} (${userId}) left world ${data.level}.`
          );
          broadcastMessage(data.level); // Notify other clients about the change

          // Check if the disconnected player was the current player
          if (userId === currentPlayerId[data.level]) {
            // Your existing logic to handle current player change
            if (worlds[data.level].length === 0) {
              delete currentPlayerId[data.level];
            } else {
              incrCurrentPlayer(data.level);
              broadCastPlayersTurn(data.level);
            }
          }
        });
      }
    }
  });

  connection.on("close", function () {
    console.log("Client disconnected");

    if (userWorld) {
      const disconnectedUsername = usernames[userWorld][userId];
      delete usernames[userWorld][userId];
      delete clients[userWorld][userId];
      worlds[userWorld] = worlds[userWorld].filter((id) => id !== userId);

      if (worlds[userWorld].length === 0) {
        worldStatus[userWorld] = true;
        const message = {
          type: "roomAvailable",
        };
        connection.send(JSON.stringify(message));
      }

      console.log(
        `${disconnectedUsername} (${userId}) left world ${userWorld}.`
      );
      broadcastMessage(userWorld);

      if (userId === currentPlayerId[userWorld]) {
        if (worlds[userWorld].length === 0) {
          delete currentPlayerId[userWorld];
        } else {
          incrCurrentPlayer(userWorld);
          broadCastPlayersTurn(userWorld);
        }
      }
    }
  });

  function broadcastMessage(world) {
    const userListMessage = JSON.stringify({
      type: "userList",
      users: usernames[world],
    });
    for (let userId in clients[world]) {
      clients[world][userId].send(userListMessage);
    }
  }

  function broadcastDiceResult(world, results) {
    for (let userId in clients[world]) {
      clients[world][userId].send(
        JSON.stringify({
          type: "diceResults",
          diceResult: results,
        })
      );
    }
  }

  function broadCastGameroomEntry(world) {
    for (let userId in clients[world]) {
      clients[world][userId].send(
        JSON.stringify({
          type: "enterGameRoom",
        })
      );
    }
  }

  function broadCastPlayersTurn(world) {
    for (let userId in clients[world]) {
      const client = clients[world][userId];
      if (userId === currentPlayerId[world]) {
        console.log(`${usernames[world][userId]} 's turn`);
        client.send(
          JSON.stringify({
            type: "yourTurn",
          })
        );
      } else {
        client.send(
          JSON.stringify({
            type: "anotherPlayer",
            currentPlayer: usernames[world][currentPlayerId[world]],
          })
        );
      }
    }
  }

  function broadcastMoving(world, resultDice) {
    for (let userId in clients[world]) {
      clients[world][userId].send(
        JSON.stringify({
          type: "moveToken",
          player: currentPlayerId[world],
          result: resultDice,
        })
      );
    }
  }

  function incrCurrentPlayer(world) {
    const currentPlayerIndex = worlds[world].indexOf(currentPlayerId[world]);
    currentPlayerId[world] =
      worlds[world][(currentPlayerIndex + 1) % worlds[world].length];
  }
});
